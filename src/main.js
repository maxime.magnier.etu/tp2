import data from "./data.js";
import Component from "./components/Component.js"
import PizzaThumbnail from "./components/PizzaThumbnail.js"

const title = new Component( 'h1', null, ['La', ' ', 'carte'] );
document.querySelector('.pageTitle').innerHTML = title.render();

const pizzaThumbnail = new PizzaThumbnail(data[0]);
document.querySelector( '.pageContent' ).innerHTML = pizzaThumbnail.render();


/*const c = new Component(
	'article',
	{name:'class', value:'pizzaThumbnail'},
	[
		new Img('https://images.unsplash.com/photo-1532246420286-127bcd803104?fit=crop&w=500&h=300'),
		'Regina'
	]
);
document.querySelector( '.pageContent' ).innerHTML = c.render();*/


/*const img = new Img('https://images.unsplash.com/photo-1532246420286-127bcd803104?fit=crop&w=500&h=300');
document.querySelector( '.pageContent' ).innerHTML = img.render();*/