import Component from "./Component.js"
import Img from "./Img.js"

export default class PizzaThumbnail{

    constructor(data){
        this.name = data.name;
        this.price_small = data.price_small;
        this.price_large = data.price_large;
        this.image = data.image;
    }

    render(){
        const liSmall = new Component('li', null, `Prix petit format : ${new Intl.NumberFormat('de-DE', { style: 'currency', currency: 'EUR' }).format(this.price_small)}`)
        const liLarge = new Component('li', null, `Prix petit format : ${new Intl.NumberFormat('de-DE', { style: 'currency', currency: 'EUR' }).format(this.price_large)}`)
        const ul = new Component('li', null, [liLarge, liSmall])
        const h4 = new Component('h4', null, this.name)
        const section = new Component('section', null, [h4, ul])
        const img = new Img(this.image)
        const a = new Component('a', {name:'href', value: this.image}, [img, section])
        const article = new Component('article', {name:'class', value: 'pizzaThumbnail'}, [a])
        return article.render();
    }
}