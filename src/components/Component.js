export default class Component{
	tagName
	children
	attribute
	constructor(tagName, attribute, children){
		this.tagName = tagName
		this.children = children
		this.attribute = attribute
	}

	render(){
		if(this.children === null || this.children === undefined){
			return `<${this.tagName} ${this.attribute.name}=\"${this.attribute.value}\"/>`
		}else{
			if(this.attribute != null && this.attribute != undefined) {
				return `<${this.tagName} ${this.attribute.name} =\"${this.attribute.value}\"}>${this.renderChildren()}</${this.tagName}>`
			}
		}
		return `<${this.tagName}>${this.renderChildren()}</${this.tagName}>`
	}

	renderChildren(){
		var res = "";

		if(this.children instanceof Array){
			this.children.forEach(element => {
				if(element instanceof Component){
					console.log(element.render())
					res += element.render();
				}else{
					res += element;
				}
			});
		}else{
			res += this.children;
		}
		console.log(res)
		return res;
	}

	
}